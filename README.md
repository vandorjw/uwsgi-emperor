# Documentation
I am currently testing everything manually. I'll attempt to create a uwsgi.rpm package if I get it working.
Following these steps should make everything work correctly.

##SELINUX
We need to set the permissions correctly.

For /etc/init.d/uwsgi

    `sudo semanage fcontext -a -t initrc_exec_t -r s0 "/etc/init.d/uwsgi"
    `sudo restorecon /etc/init.d/uwsgi
	
For /etc/default/uwsgi

    `sudo semanage fcontext -a -t etc_t -r s0 "/etc/default/uwsgi"
	`sudo restorecon /etc/default/uwsgi
	


